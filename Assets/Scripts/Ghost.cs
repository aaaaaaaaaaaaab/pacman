﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{


    NavMeshAgent _navAgent;
    GameObject _player;

    [SerializeField]
    GameObject GhostMesh;
    static public int points = 200;
    void Awake()
    {

        _navAgent = GetComponent<NavMeshAgent>();
    }

    private void OnDisable()
    {
        Destroy(GhostMesh);
    }

    // Use this for initialization
    void Start() { 

_player = GameObject.FindGameObjectWithTag("Player");
    _navAgent.SetDestination(_player.transform.position);
    
    }
    
    // Update is called once per frame  
    void Update()
{
        _navAgent.SetDestination(_player.transform.position);
        GhostMesh.transform.position = transform.position;

}
}
