﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacManMovement : MonoBehaviour
{
    [SerializeField]
    float MovementSpeed;

    public int TotalPoints;

    public int Lives = 3;

    AudioSource _audioSource;


    [SerializeField]

    AudioClip _audioMov;
    [SerializeField]
    AudioClip _audioDeath;

    bool _hasPowerup;


    float _PowerUpElapsedTime = 0;


    float _PowerupDuration = 10;
    int _eateGhost;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }




    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        //Debug.Log("h: " + h + " - v: " + v);
        if (h != 0)
        {
            transform.Translate(Vector3.right * MovementSpeed * h);
        }
        else
        {

            transform.Translate(Vector3.forward * MovementSpeed * v);
        }

        if (h != 0 || v != 0)
        {
            if (!_audioSource.isPlaying)
                _audioSource.PlayOneShot(_audioMov);
        }

        if(_hasPowerup)
        {

            _PowerUpElapsedTime += Time.deltaTime;

            if (_PowerUpElapsedTime >= _PowerupDuration)
            {
                _eateGhost = 0;
                _hasPowerup = false;
            }
        }   
    }




    void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.tag == "pill")
        {
            OnEatPill(other);
        }
        if (other.gameObject.tag == "Ghost")
        {
            Debug.Log("INCONTRO FANTASMA");
            if (!_hasPowerup) 
                Onhit();
            

            else
            {

                _eateGhost++;

                TotalPoints += (int)Mathf.Pow(2, _eateGhost -1)*  Ghost.points;
                Destroy(other.gameObject);
            }
        }

        




    }

    private void OnEatPill(Collider other)
    {
        Debug.Log("GNAM!");
        Pill pill = other.gameObject.GetComponent<Pill>();
        TotalPoints += pill.points;

        if (pill is powerup)
        {
            Debug.Log("POWER UP!!!");
            _hasPowerup = true;
            _PowerUpElapsedTime = 0;


        }
        Destroy(other.gameObject);
    }

    private void Onhit()
    {
        Debug.Log("GAME OVER");

        if (_audioSource.isPlaying)
            _audioSource.Stop();
        _audioSource.PlayOneShot(_audioDeath);
        Lives -= 1;
    }
}