﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIScore : MonoBehaviour
{




    [SerializeField]
    PacManMovement pacman;
    // Use this for initialization
    [SerializeField]
    Text _Text;

    private void Awake()
    {
        _Text = GetComponent<Text>();
    }


    void Start()
    {

    }// Update is called once per frame


    void Update()
    {
        _Text.text = pacman.TotalPoints.ToString();
    }
}
